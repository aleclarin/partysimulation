import java.util.Random;
import java.util.concurrent.*;


/**
 * @author Alec
 *
 */
public class PersonMover extends Thread {
	public static int IDcounter=0;
	private Person thisPerson;
	public static RoomGrid grid; //shared grid
	private Random rand;
	private PeopleCounter counter;

	public volatile static boolean done=false; //add stop button

	private int ID; //thread ID for debugging

	private boolean paused = false;	//true if the program is paused, false otherwise

	public static Object maxPplLock = new Object();		//Lock to enforce max number of people allowed in the room
	public static Object entranceLock = new Object();	//Lock to ensure that only one person enters through the entrance
	
	public static int maxPpl;

	//Keeps track of the order in which the people arrived
	public static LinkedBlockingQueue<PersonMover> arrOrder = new LinkedBlockingQueue<PersonMover>();	

	PersonMover( Person janeDoe) {
		thisPerson = janeDoe;
		rand = new Random();	

		synchronized (PersonMover.class) {
			ID=IDcounter;
			PersonMover.IDcounter++;
		}
	}

	PersonMover( Person creature, PeopleCounter score) {
		this(creature);
		this.counter=score;
	}



	private void headForRefreshments() {  //next move towards bar
		//no need to change this function
		int x_mv=-1;
		int y_mv=-1;
		GridBlock nextBlock =null;
		while (nextBlock==null) { // repeat until hit on empty cell

			x_mv= rand.nextInt(3)-1+thisPerson.getX();
			if (x_mv<0) x_mv=0;

			y_mv=thisPerson.getY()+1;
			if (y_mv<0) y_mv=0;		
			else if (y_mv>=grid.getMaxY()) y_mv=grid.getMaxY()-1;	

			if (!((x_mv==thisPerson.getX())&&(y_mv==thisPerson.getY()))) {
				nextBlock=grid.getBlock(x_mv,y_mv);
			} 
		}	
		thisPerson.moveToBlock(nextBlock);
	}

	private void mingle() { //random next move
		//no need to change this function
		int x_mv=-1;
		int y_mv=-1;
		GridBlock nextBlock =null;
		while (nextBlock==null) { // repeat until hit on empty cell
			x_mv= rand.nextInt(3)-1+thisPerson.getX();
			if (x_mv<0) x_mv=0;
			y_mv=rand.nextInt(3)-1+thisPerson.getY();;
			if (y_mv<0) y_mv=0;		
			if (!((x_mv==thisPerson.getX())&&(y_mv==thisPerson.getY()))) {
				//System.out.println("moving from x="+x+" y="+y); //debug
				//System.out.println("moving to x="+x_mv+" y="+y_mv); //debug
				nextBlock=grid.getBlock(x_mv,y_mv);
			} 
		}	
		thisPerson.moveToBlock(nextBlock);

	}

	public void run() {

		boolean moved = false;	//true if this person has moved since they entered, false otherwise
		GridBlock firstBlock = grid.getEntranceBlock(); //enter through entrance

		try {
			sleep(rand.nextInt(10000)); //time till arriving at party

			//Add this PersonMover to the queue that stores arrival order
			arrOrder.add(this);
			
			isPaused();		//Check if the program was paused during sleep

			counter.personArrived(); //add to counter

			//Section that checks if the person should enter
			//Uses the entranceLock to make threads wait
			synchronized(entranceLock) {
				//Wait while the entrance is occupied
				//or while this person is not the next in line (i.e. they did not arrive the earliest out of those waiting)
				while(firstBlock.getStatus() || !(PersonMover.arrOrder.peek().ID == this.ID)) {
					entranceLock.wait();
				}
				firstBlock.waitBlock(); //occupy entrance once it is this person's turn to enter
			}

			//Section that ensures the maximum room capacity
			//Uses the maxPplLock to make threads wait
			//There is only ever one person waiting at this point at a given time
			synchronized(maxPplLock) {
				//Wait while the room is at maximum capacity
				while(counter.getInside() >= maxPpl) {
					maxPplLock.wait();	
				}
			}	
			thisPerson.initBlock(firstBlock);
			counter.personEntered(); //add to counter
			sleep(thisPerson.getSpeed());
		} catch (InterruptedException e1) {
			done=true;
		}

		while ((!done)&&(thisPerson.inRoom())) {	
			isPaused(); //Check if the program is paused
			try {	
				boolean atEntrance = false; //true if the person is occupying the entrance block, false otherwise
				if(grid.getEntranceBlock().equals(thisPerson.currentBlock)) {
					atEntrance = true;
				}
				if (thisPerson.thirsty()) {
					if (thisPerson.atRefreshmentStation()) {
						sleep(thisPerson.getSpeed()*4);//drinking for a while
						thisPerson.drink();
					}
					else headForRefreshments();
				} 
				else if (thisPerson.atExit()) {
					thisPerson.leave();
				} 
				else {
					mingle();
				}
				sleep(thisPerson.getSpeed());

				if(atEntrance) {	//Check if this person is at the entrance
					if(!moved) {	//True if this person just entered and is moving to another block for the first time, false otherwise
						//Remove the head from arrOrder
						arrOrder.poll();
						moved = true;
					}
					//Section that notifies that this person has moved off the entrance block
					//Uses the entranceLcok to notify waiting threads
					synchronized(entranceLock) {
						entranceLock.notifyAll();
					}
				}

			}			

			catch (InterruptedException e) {
				System.out.println("Thread "+this.ID + "interrrupted."); 
				done=true;
			} 
		}
		if(counter.getInside() >= maxPpl) {
			//Section that notifies that there is space in the room for the next person
			//Uses the maxPplLock to notify waiting threads
			synchronized(maxPplLock) {
				counter.personLeft(); //add to counter
				maxPplLock.notify();
			}	
		} else {
			counter.personLeft(); //add to counter
		}
	}

	//Sets the paused variable to true
	public void pause(){
		paused = true;
	}

	//Checks if the program is paused. 
	//If paused, this thread will wait()
	private void isPaused() {
		if(paused) {
			synchronized (this) {
				while (paused) {
					try {
						this.wait();
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
				}
			}
		}
	}

	//Method that notifies this thread if it is waiting
	public void cont() {
		synchronized(this) {
			paused = false;
			this.notify();
		}
	}
}
